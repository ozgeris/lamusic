<div class="left menubar">
    <a href="{{route('main')}}">
        <img class="logo" src="/img/logo.png" />
    </a>
    <div style="height:30px"></div>
    <div class="header">Меню</div>
    <ul class="menu">
        <li {{setActive('main')}}>
            <a {{setHref('main')}}><i class="icon-home"></i>&nbsp;Главная</a>
        </li>
        <li {{setActive('news')}}>
            <a {{setHref('news')}}><i class="icon-news"></i>&nbsp;Новости</a>
        </li>
        <li {{setActive('recents')}}>
            <a {{setHref('recents')}}><i class="icon-recent"></i>&nbsp;Новинки</a>
        </li>
        <li {{setActive('artists')}}>
            <a {{setHref('artists')}}><i class="icon-artists"></i>&nbsp;Исполнители</a>
        </li>
        <li {{setActive('upload')}}>
            <a {{setHref('upload')}}><i class="icon-add-track"></i>&nbsp;Добавить трек</a>
        </li>
        <li {{setActive('forums')}}>
            <a {{setHref('forums')}}><i class="icon-forum"></i>&nbsp;Форум</a>
        </li>
        <li {{setActive('top')}}>
            <a {{setHref('top')}}><i class="icon-top-100"></i>&nbsp;ТОП 100</a>
        </li>
        <li {{setActive('instrumentals')}}>
            <a {{setHref('instrumentals')}}><i class="icon-instrumental"></i>&nbsp;Минусовки</a>
        </li>
    </ul>
    <div style="height:30px"></div>
    <div class="header">топ 10</div>
    <ul class="top">
        @for($i=0;$i< 10;$i++)
        <li>
        <a href={{route('song',['slug'=>$i])}}><i class="icon-song"></i>&nbsp;Арман ай арман</a>
        </li>
        @endfor
    </ul>
    <div style="height:30px"></div>
    <div class="header">Соц сети</div>
    <div class="social">
        <a href="#"><i class="icon-youtube gold"></i></a>
        <a href="#"><i class="icon-insta gold"></i></a>
        <a href="#"><i class="icon-vk gold"></i></a>
        <div class="clear"></div>
    </div>
</div>