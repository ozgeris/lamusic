@extends('default')
@section('title','Регистрация')
@section('content')
<div class="reg shaded">
    <div style="display:inline-block;">
        <form action={{route('register')}} method="post">
            <div class="input-row">
                <label for="">Email</label>
                <input type="email" name="email" required placeholder="Ваш e-mail" />
            </div>
            <div class="input-row">
                <label for="">Псевдоним</label>
                <input type="text" name="name" required placeholder="Ваш псевдоним на форуме" />
            </div>
            <div class="input-row">
                <label for="">Пароль</label>
                <input type="password" name="password" required placeholder="Ваш пароль" />
            </div>
            <div class="input-row">
                <label for="">Повторите пароль</label>
                <input type="password" name="password2" required placeholder="Повторите еше раз" />
            </div>
            <div class="input-row">
                <input type="submit" name="register" value="Регистрация" />
            </div>
        </form>
    </div>
</div>
@stop