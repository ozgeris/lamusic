@php
$alphKaz=['А','Ә','Б','В','Г','Ғ','Д','Е','Ё','Ж','З','И','Й','К','Қ','Л','М','Н','Ң','О','Ө','П','Р','С','Т','У','Ұ','Ү','Ф','Х','Һ','Ц','Ч','Ш','Щ','Ъ','Ы','І','Ь','Э','Ю','Я'];
$alphEn=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',];
function mock(){
return [
'name'=>(rand(0,1)==0?'Нюша':'Қайрат'),
'img'=>'img/art-'.(rand(0,1)).'.png'
];
}
@endphp
@extends('default')
@section('title','Песни')
@section('content')
<div class="songs-block">
    <h4>Поиск по алфавиту</h4>
    @foreach($alphKaz as $letter)
    <a href="#" class="letter">{{$letter}}</a>
    @endforeach
    <br />
    <br />
    @foreach($alphEn as $letter)
    <a href="#" class="letter">{{$letter}}</a>
    @endforeach
    <div class="songs">
        @for($i=0;$i<20;$i++)
            @include('component.song-item')
        @endfor
        <div class="clear"></div>
    </div>
</div>
@include('component.show-more',['type'=>'songs'])
@stop