<div class="top">
    <div class="left">
        <img src="/img/certified.png" style="width:100%;vertical-align:middle" />
    </div>
    <div class="middle" style="">
        <input class="search" type="text" placeholder="трек, исполнитель" />
        <i class="icon-search gold"></i>
        <a {{setHref('favorites')}}>
            <i class="icon-like gold"></i>
            Избранные
        </a>
    </div>
    <div class="right" style="line-height:200%;float:right;width:auto;">
        <a {{setHref('login')}}>
            <i class="icon-user gold"></i>
            Вход
        </a>
        <div class="lang">
            <a href="#" class="active">қаз</a><a href="#">рус</a>
        </div>
    </div>
    <div class="clear"></div>
</div>