@extends('default')
@section('title','Логин')
@section('content')
<div class="reg shaded">
    <div style="display:inline-block;">
        <form action={{route('login')}} method="post">
            <div class="input-row">
                <label for="">Email</label>
                <input type="email" name="email" required placeholder="Ваш e-mail" />
            </div>
            <div class="input-row">
                <label for="">Пароль</label>
                <input type="password" name="password" required placeholder="Ваш пароль" />
            </div>
            <div class="input-row">
                <input type="submit" name="register" value="Логин" />
            </div>
        </form>
    </div>
</div>
@stop