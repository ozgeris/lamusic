<div class="show-more">
    <a href="#" class="show-more-btn" data-type="{{$type}}" data-page="0">
        <div class="holder">
            <i class="icon-more-in gold">&nbsp;</i>
            <i class="icon-more-out gold">&nbsp;</i>
        </div>
    </a>
</div>