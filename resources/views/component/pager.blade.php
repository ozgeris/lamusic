<div class="pager">
    @for($i=1;$i<=$pages;$i++) 
    @if($current!=$i)
    <a href="{{$url.$i}}" class="letter">{{$i}}</a>
    @else
    <a class="letter active">{{$i}}</a>
    @endif
    @endfor
</div>