<div class="player">
            <div class="left-part">
                <a href="#" class="play">
                    <i class="icon-play gold"></i>
                </a>
            </div>
            <div class="seek-item">
                <div class="progress">
                    <div class="current" style="width:{{rand(0,100)}}%"></div>
                </div>
                <input type="range" min="0" max="100" value="0" />
            </div>
    <div class="minutes">5:50</div>
    <div class="download">
        <a href="#"><i class="icon-download gold"></i></a>
        <span class="size gold-text">5.56 MB</span>
    </div>
    <div class="clear"></div>
</div>