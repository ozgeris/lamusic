<div class="song-item">
    <a href={{route('song',['slug'=>$i])}}>
        <div>
            <div class="left-part">
                <i class="icon-play gold"></i>
            </div>
            <div class="right-part">
                <div>
                    <div class="artist">{{$i%2==0?'Нюша':'Тимати'}}</div>
                    <div class="name">{{$i%2==0?'Кайрат любовь':'Пацан'}}</div>
                </div>
            </div>
            <div class="minutes">
                5:45
            </div>
        </div>
    </a>
</div>