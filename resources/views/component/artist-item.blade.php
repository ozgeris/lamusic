
<div class="artist-item">
    <div style="position:relative;overflow:hidden;line-height:100%;">
        <img src="{{$img}}" />
        <a href={{route('artist',['slug'=>$i])}} class="hover-btn">
            <span>ПОДРОБНЕЕ</span>
        </a>
    </div>
    <div class="artist">{{$name}}</div>
</div>