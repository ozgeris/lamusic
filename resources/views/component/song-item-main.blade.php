<div class="song-item-main">
    <div style="position:relative;overflow:hidden;line-height:100%;">
        <img src="/img/art-{{$i%2}}.png" />
        <a href={{route('song',['slug'=>$i])}} class="play">
            <span>
                <i class="icon-play gold"></i>
            </span>
        </a>
        <a href="#" class="like">
                <label>
                    <input type="checkbox" />
                    <i class="icon-like gold"></i>
                    <i class="icon-love gold"></i>
                </label>
        </a>
    </div>
    <div class="artist">
        <a href={{route('artist',['slug'=>$i])}}>{{$i%2==0?'Нюша':'Тимати'}}</a>
    </div>
    <div class="name">{{$i%2==0?'Кайрат любовь':'Пацан'}}</div>
</div>