@php
function setActive($route){
return (request()->route()->getName()==$route?'class=active':'');
}
function setHref($route,$params=[]){
return (request()->route()->getName()==$route?"$route":'href='.route($route,$params));
}
@endphp
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no" />
    <link rel="stylesheet" href="/css/app.css" />
    <script src="/1jquery-3.1.1.js"></script>
    <script>
        window.onload = function () {
            $('.show-more-btn').on('click', function (e) {
                e.preventDefault();
                var elem = $(this);
                var url = '/'+elem.attr('data-type') + '/' + elem.attr('data-page');
                $.ajax(url, {
                    method: 'GET',
                    dataType: 'json',
                    success: function (res) {
                        for (var i = 0; i < res.length; i++) {
                            if (elem.attr('data-type') == 'artists') {
                                var html = $('#temp').html();
                                html = $(html);
                                html.find('img').attr('src', res[i].img);
                                html.find('.artist').html( res[i].name);
                                $('.artists-block').append(html);
                            }
                            else if (elem.attr('data-type') == 'artist/12/songs')
                            {
                                var html = $('#temp2').html();
                                html = $(html);
                                html.find('.artist').html(res[i].artist);
                                html.find('.name').html(res[i].name);
                                $('.songs-block').append(html);
                            }
                            else
                            {
                                var html = $('#temp1').html();
                                html = $(html);
                                html.find('.artist').html(res[i].artist);
                                html.find('.name').html(res[i].name);
                                $('.songs-block').append(html);
                            }
                            
                        }
                    }
                });
            });
        }
    </script>
    <script type="text/template" id="temp">
	<div class="artist-item">
		<a href="#">
        <div style="position:relative;overflow:hidden;line-height:100%;">
			<img src="" />
			<a href="#" class="hover-btn"><span>ПОДРОБНЕЕ</span></a>
		</div>
		<div class="artist"></div>
                    </a>
	</div>
    </script>
    <script type="text/template" id="temp2">
                <div class="song-item-main">
    <div style="position:relative;overflow:hidden;line-height:100%;">
        <img src="/img/art-{{0}}.png" />
        <a href="#" class="play">
            <span>
                <i class="icon-play gold"></i>
            </span>
        </a>
        <a href="#" class="like">
                <label>
                    <input type="checkbox" />
                    <i class="icon-like gold"></i>
                    <i class="icon-love gold"></i>
                </label>
        </a>
    </div>
    <div class="artist"></div>
    <div class="name"></div>
</div>
    </script>
    <script type="text/template" id="temp1">
        <div class="song-item">
    <a href="#">
        <div>
            <div class="left-part">
                <i class="icon-play gold"></i>
            </div>
            <div class="right-part">
                <div>
                    <div class="artist"></div>
                    <div class="name"></div>
                </div>
            </div>
            <div class="minutes">
                5:45
            </div>
        </div>
    </a>
</div>
    </script>
</head>
<body>
    <div class="body">
        <div class="top-ad">
            <div style="background:gray;font-size:2em;text-align:center;line-height:400%">AD HERE</div>
        </div>
        @include('header')
        <div class="main">
            @include('menubar')
            <div class="middle">
                @if(request()->route()->getName()!='main' )
                <div>
                    <a href={{ url()->previous() }} class="gold-text">
                        <i class="icon-back gold"></i>&nbsp; Назад
                    </a>
                </div>
                <div class="pre-title">@yield('pre-title')</div>
                <h1>@yield('title')</h1>
                @endif
                @yield('content')
            </div>
            @include('rightbar')
            <div class="clear"></div>
        </div>
        @include('footer')
    </div>
</body>
</html>