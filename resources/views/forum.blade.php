@php
$faker = Faker\Factory::create();
@endphp
@extends('default')
@section('title','Форум по имени солнце')
@section('pre-title')
@section('content')
<div class="forum-block">
    @for($i=0;$i < 10;$i++)
    <div class="message-item shaded little">
        <div class="author">
                Adam
                <br />
                Пользователь
        </div>
        <div class="text">
            <span class="message">asdadawfefawefaewfaewfafwefawfae</span>
            <br />
            <br />
            <span class="date">{{$faker->dateTimeThisMonth('now')->format('H:i d.m.Y')}}</span>
        </div>
        <div class="clear"></div>
    </div>
    @endfor
</div>
@include('component.pager',['pages'=>20,'current'=>10,'url'=>request()->url().'/'])
@stop