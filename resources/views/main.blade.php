@extends('default')
@section('content')
<div class="news-block">
    <div class="news big">
        <a href="#">
            <div class="item">

                <div class="overlay">
                    <div class="title">Самые новые новости</div>
                    <div class="desc">В связи с землетрясением в Филиппинах казахстанских туристов просят соблюдать требования безопасности</div>
                </div>
            </div>
        </a>
    </div>
    <div class="small-container">
        @for($i=0;$i < 3;$i++)
        <div class="news small">
            <a href="#">
                <div class="item">
                    <div class="overlay">
                        <div class="title">Қайрат Нуртас стал халтурщиком</div>
                    </div>
                </div>
                </a>
</div>
        @endfor
    </div>
    <div class="clear"></div>
</div>

<div class="new-songs">
    <h2>Новинки</h2>
    @for($i=0;$i < 16;$i++)
        @include('component.song-item-main',['i'=>$i])
    @endfor
    <div class="clear"></div>
</div>
@endsection