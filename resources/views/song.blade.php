@extends('default')
@section('title')
@section('pre-title')
@section('content')
<div class="song-block">    
    <div class="song-info">
        <div class="song-item-main shaded little">
            <div style="position:relative;overflow:hidden;line-height:100%;">
                <img src="/img/art-1.png" />
            </div>
        </div>
        <div class="text">
            <h1>Арман-ай - Музыка</h1>
            <span>Предложена учёным Карлом Ландштейнером в 1900 году. Известно несколько основных групп аллельных генов этой системы: A¹, A², B и 0. Генный локус для этих аллелей находится на длинном плече хромосомы 9. Основными продуктами первых трёх генов — генов A¹, A² и B, но не гена 0 — являются специфические ферменты гликозилтрансферазы, относящиеся к классу трансфераз. Эти гликозилтрансферазы переносят специфические сахара — N-ацетил-D-галактозамин (англ.)русск. в случае гликозилтрансфераз A¹ и A² типов, и D-галактозу в случае гликозилтрансферазы B-типа.</span>
        </div>
        <div class="clear"></div>
    </div>
    @include('component.player')
    @include('component.social')
    <h2>Другие песни исполнителя</h2>
    <div class="songs-block">
        @for($i=0;$i<12;$i++)
            @include('component.song-item',['i'>$i])
    @endfor
    </div>
</div>
@include('component.show-more',['type'=>'songs'])
@stop