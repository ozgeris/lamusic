@extends('default')
@section('title','Тимати')
@section('pre-title')
<div class="artist-pre">
    <br />
    <img class="shaded little" src="/img/tima.jpg" />
</div>
@stop
@section('content')
<div class="artist-block">
    <p class="desc">
        Тиму́р Ильда́рович Юну́сов (тат. Тимур Илдар улы Юнусов, род. 15 августа 1983, Москва), более известный как Ти́мати — российский исполнитель, музыкальный продюсер, актёр и предприниматель[2], выпускник «Фабрики звёзд 4». Заслуженный артист Чеченской Республики (2014). Сотрудничал с такими американскими исполнителями, как Snoop Dogg, Баста Раймс, Дидди и его группой Diddy – Dirty Money, Xzibit, Марио Уайнэнс (англ.)русск., Fat Joe, Ив, Крейг Дэвид, Тимбалэнд и Flo Rida.
«Welcome to St. Tropez (DJ Antoine Remix)» — сремикшированная швейцарским DJ Antoine версия песни «Welcome to St. Tropez» попала во многие российские чарты, чарты Европы,[3] а также имеет более 133 миллионов просмотров на YouTube[4][значимость факта?].
В июне 2012 года состоялся мировой релиз первого англоязычного альбома Тимати — «SWAGG»[5]. В тот же месяц вышел клип Тимати «ДавайДосвидания» совместно с участием таких рэперов, как L'One, ST, Nel, Jenee, 5 Плюх и Миша Крупин. Этот сингл был просмотрен на YouTube более 31,5 миллиона раз[6].
    </p>
    <h2>Песни</h2>
<div class="songs-block">
    @for($i=0;$i<12;$i++)
        @include('component.song-item-main',['i'=>$i])
    @endfor
    </div>
</div>
    @include('component.show-more',['type'=>'artist/12/songs'])
@stop