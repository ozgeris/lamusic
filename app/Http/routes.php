<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    return view('main');
})->name('main');
Route::get('/news', function () {
    return view('main');
})->name('news');
Route::get('/favorites', function () {
    return view('main');
})->name('favorites');

Route::get('/recents', function () {
    return view('main');
})->name('recents');
Route::get('/top', function () {
    return view('main');
})->name('top');
Route::get('/instrumentals', function () {
    return view('main');
})->name('instrumentals');
Route::get('/upload', function () {
    return view('main');
})->name('upload');
Route::get('/forums', function () {
    return view('forums');
})->name('forums');
Route::get('/forum/{slug}', function ($slug) {
    return view('forum');
})->name('forum');
Route::get('/register', function () {
    return view('registration');
})->name('register');
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::get('/artists', function () {
    return view('artists');
})->name('artists');
Route::get('/songs', function () {
    return view('songs');
})->name('songs');
Route::get('/song/{slug}', function ($slug) {
    return view('song',['slug'=>$slug]);
})->name('song');
Route::get('/artist/{slug}', function ($slug) {
    return view('artist',['slug'=>$slug]);
})->name('artist');
Route::get('/artist/{slug}/songs/{page}', function ($slug,$page) {
    function mock(){
        return [
'artist'=>(rand(0,1)==0?'Нюша':'Қайрат'),
'name'=>(rand(0,1)==0?'Olen 1':'Olen 2')
        ];
    }
    $array=[];
    for($i=0;$i<20;$i++)
    {
        array_push($array,mock());
    }
    return response()->json($array);
})->name('artist-song');

Route::get('/artists/{page}', function ($page) {
    function mock(){
        return [
'name'=>(rand(0,1)==0?'Нюша':'Қайрат'),
'img'=>'img/art-'.(rand(0,1)).'.png'
        ];
    }
    $array=[];
    for($i=0;$i<12;$i++)
    {
        array_push($array,mock());
    }
    return response()->json($array);
});
Route::get('/songs/{page}', function ($page) {
    function mock(){
        return [
'artist'=>(rand(0,1)==0?'Нюша':'Қайрат'),
'name'=>(rand(0,1)==0?'Olen 1':'Olen 2')
        ];
    }
    $array=[];
    for($i=0;$i<20;$i++)
    {
        array_push($array,mock());
    }
    return response()->json($array);
});
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */

Route::group(['middleware' => ['web']], function () {
    //
});
